import 'dart:io';

int add(int x, int y) {
  int s;
  s = x + y;
  return s;
}
int subtract(int x, int y) {
  int s;
  s = x - y;
  return s;  
}
int multiply(int x, int y) {
  int s;
  s = x * y;
  return s;  
}
double divide(double x, double y) {
  double s;
  s = x / y;
  return s;  
}

void main() {
  print("MENU");
  print(
      "Select the choice you want to perform : \n 1. ADD \n 2. SUBTRACT \n 3. MULTIPLY \n 4. DIVIDE \n 5. EXIT \n");
  print("Choice you want to enter :");
  var ops = stdin.readLineSync();
  switch (ops) {
    case "1":
      {
        print(' Enter the value of x : ');
        int x = int.parse(stdin.readLineSync()!);
        print(' Enter the value of y : ');
        int y = int.parse(stdin.readLineSync()!);
        int s = add(x, y);
        print(' The sum of these two variables is ');
        print(s);
        

      }
      break;
    case "2":
      {
        print(' Enter the value of x : ');
        int x = int.parse(stdin.readLineSync()!);
        print(' Enter the value of y : ');
        int y = int.parse(stdin.readLineSync()!);
        int s = subtract(x, y);
        print(' The sum of these two variables is ');
        print(s);
      }
      break;
    case "3":
      {
        print(' Enter the value of x : ');
        int x = int.parse(stdin.readLineSync()!);
        print(' Enter the value of y : ');
        int y = int.parse(stdin.readLineSync()!);
        int s = multiply(x, y);
        print(' The sum of these two variables is ');
        print(s);
      }
      break;
    case "4":
      {
        print(' Enter the value of x : ');
        double x = double.parse(stdin.readLineSync()!);
        print(' Enter the value of y : ');
        double y = double.parse(stdin.readLineSync()!);
        double s = divide(x, y);
        print(' The sum of these two variables is ');
        print(s);
      }
      break;
    case "5":
      {
        break;
      }
      break;
    default:
      {
        print("Invalid choice");
      }
      break;
  }
}
